﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;

namespace TempConvert
{
    [Activity(Label = "TempConvert", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        EditText tbTemp;
        TextView lblConvert;
        Button convertButton;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            tbTemp = FindViewById<EditText>(Resource.Id.etTemp);
            lblConvert = FindViewById<TextView>(Resource.Id.convertedLabel);
            convertButton = FindViewById<Button>(Resource.Id.btnConvert);

            convertButton.Click += ConvertTemp;
        }
        public void ConvertTemp(object obj, EventArgs e) {
            int f = Int32.Parse(tbTemp.Text);
            double c = (f - 32) * 5 / 9;
            lblConvert.Text = "Temperature in Celsius: " + c;

        }
    }
}

